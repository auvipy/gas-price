from bs4 import BeautifulSoup
import requests
import pandas as pd


response = requests.get('http://www.eia.gov/dnav/ng/hist/rngwhhdm.htm')
tables = BeautifulSoup(response.content, 'lxml').findAll('table')
# [print(t) for t in tables]
# print(tables[4])
gas_price_table = list(tables)[4]
#print(type(gas_price_table))
df = pd.read_html(str(gas_price_table))
print(type(df))
[print(f) for f in df]

# print(pd.concat([f for f in df], axis=1, join_axes=[df[0].index]))



"""
df1 = pd.DataFrame({'A': ['A0', 'A1', 'A2', 'A3'],
                    'B': ['B0', 'B1', 'B2', 'B3'],
                    'C': ['C0', 'C1', 'C2', 'C3'],
                    'D': ['D0', 'D1', 'D2', 'D3']},
                    index=[0, 1, 2, 3])


df4 = pd.DataFrame({'B': ['B2', 'B3', 'B6', 'B7'],
                    'D': ['D2', 'D3', 'D6', 'D7'],
                    'F': ['F2', 'F3', 'F6', 'F7']},
                    index=[2, 3, 6, 7])

result = pd.concat([df1, df4], axis=1, join_axes=[df1.index])
print(result)
"""
