import requests
import xlrd
import datetime
from dateutil.relativedelta import relativedelta
import csv
import json
import pandas as pd
import plotly
import plotly.graph_objs as go


# apparantly the url to xls file stays the same
URL = "http://www.eia.gov/dnav/ng/hist_xls/RNGWHHD{}.xls"


def extract(url, history):
    """ Gets and saves csv """

    # get the file
    print("Downloading from: " + url.format(history))
    response = requests.get(url.format(history))

    # file names based on history
    xls_name = "spreadsheet_{}.xls".format(history)
    csv_name = "dataset_{}.csv".format(history)
    json_name = "dataset_{}.json".format(history)

    # save to spreadsheet.xls
    with open(xls_name, 'wb') as xls_file:
        if not response.ok:
            print("Couldn't find the file")
        else:
            xls_file.write(response.content)
            print("File downloaded to: " + xls_name)


    # convertion to csv
    book = xlrd.open_workbook(xls_name)
    # print("The number of worksheets is {0}".format(book.nsheets))
    # print("Worksheet name(s): {0}".format(book.sheet_names()))
    # select sheet 1
    sheet = book.sheet_by_index(1)
    # print sheet 1 info
    # print("{0} {1} {2}".format(sh.name, sh.nrows, sh.ncols))
    # print("Cell D30 is {0}".format(sh.cell_value(rowx=29, colx=3)))


    # saving to csv
    data_list = []
    for row in range(3, sheet.nrows):
        # print(sh.row(row))
        # takes apart a row
        dt, btu = sheet.row(row)
        # converts raw xlrd date format to tuple, then to standard Python datetime
        dt = datetime.datetime(*xlrd.xldate_as_tuple(float(dt.value), book.datemode)).date()
        # to float
        btu = float(btu.value)
        data_list.append({
                'date':str(dt),
                'value': btu
            })

        # csv_file.write(str(dt)+","+str(btu)+"\n")
        # print("{} - {}".format(dt, btu))
        # print("File converted to: " + csv_name)

    to_csv(data_list, history)
    to_json(data_list, history)
    # [print(data) for data in data_list]
    return data_list


def to_csv(data_list, history):
    csv_name = "dataset_{}.csv".format(history)
    columns = ['date', 'value']
    with open(csv_name, "w") as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=columns, extrasaction='ignore')
        writer.writeheader()
        for entry in data_list:
            writer.writerow(entry)
            # print(entry)
        print("File converted to CSV as: " + csv_name)


def to_json(data_list, history):
    json_name = "dataset_{}.json".format(history)
    # columns = ['date', 'value']
    with open(json_name, "w") as json_file:
        # print(json.dumps(data_list))
        json_file.write(json.dumps(data_list))
        print("File converted to JSON as: " + json_name)




def make_plot(history):
    """ Plotting is secondary """
    csv_name = "dataset_{}.csv".format(history)
    # dataframe to hold the time series
    df = pd.read_csv(csv_name, sep=',', parse_dates=[0], header=None,names=['Date','Value'])
    # print(df)

    # Plotting with Plotly
    data = [
        go.Scatter(
            x=df['Date'],
            y=df['Value']
            )
        ]

    layout = go.Layout(
        title="Henry Hub Natural Gas Spot Price (Dollars per Million Btu)",
        xaxis={'title':'Dates'},
        yaxis={'title':'Values'}
    )

    fig = go.Figure(data=data, layout=layout)
    plotly.offline.plot(fig, filename='plot_{}.html'.format(history))


extract(URL, 'a')   # annual
extract(URL, 'm')   # monthly
extract(URL, 'w')   # weekly
extract(URL, 'd')   # daily

make_plot('a')   # annual
make_plot('m')   # monthly
make_plot('w')   # weekly
make_plot('d')   # daily
